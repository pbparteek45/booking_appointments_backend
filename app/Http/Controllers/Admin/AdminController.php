<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Hash;
use App\User;
use App\Role;
use Validator;
use App\Mail\VerificationEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;


class AdminController extends Controller
{
   public function adminLogin(){

	  $validator = Validator::make(Input::all(),
	    [
	      'email' => 'required',
	      'password' => 'required|min:5'
	    ]);

	  if($validator->fails()){
	    return repsonse()->json($validate->errors());
	  }

	  try{
	    if(!$token = auth()->attempt([
	      'email'=> Input::get('email'),
	      'password'=>Input::get('password'),
	    ])
	  ){
	      return response()->json(['error'=>'invalid crediantials']);
	    }
	  } catch(JWTException $e){
	    return response()->json(['error'=>'could not create token']);
	  }
	  $user = auth()->user();
	  // $user = User::with('roles');
	  $role = User::find($user->id)->roles()->orderBy('name')->first();
	  if($role->name==='admin'){
	    return redirect('api/admin/dashboard');
	    return response()->json(compact('token'));
	  }
	  else{
	    abort('401','this action is unauthorized');
	  }
	}

	public function userList(){

		$user = Role::where('name','user')->first();

		$roles = Role::find($user->id)->users()->get();

		return response($roles);	
	}

	public function merchantList(){

		$user = Role::where('name','merchant')->first();

		$roles = Role::find($user->id)->users()->get();

		return response($roles);
	}
}
