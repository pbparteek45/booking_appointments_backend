<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use Hash;
use App\User;
use App\Role;
use Validator;
use App\Mail\VerificationEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\response;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class MerchantController extends Controller
{
    public function merchantSignup(Request $request){
     
     $validator = Validator::make($request->all(), [
       'first_name' => 'required|alpha|min:3|max:20',
       'last_name' => 'nullable|alpha|min:3|max:20',
       'email'=> 'required|string|email|max:255|unique:users',
       'password'=> 'required|string|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|min:5|max:20',
       'confirm_password' => 'required|same:password',
       'mobile_no'=> 'required|numeric|min:10'
     ]);

     if ($validator->fails()) {
      $errors = $validator->errors();
      return response()->json(['errors'=>$errors]);
    }

    $user = new User([
      'first_name' => $request->get('first_name'),
      'last_name' => $request->get('last_name'),
      'email'=> $request->get('email'),
      'password'=> Hash::make($request->get('password')),
      'mobile_no'=> $request->get('mobile_no'),
      'token'=> str_random(40)
    ]); 

    $user->save();

    $user->roles()->attach(Role::where('name','merchant')->first());


    Mail::to($user->email)->send(new VerificationEmail($user));

    return response()->json(['success'=>'verification email link has sent to your emailid. Please verify email id before login']);
	}


	public function merchantLogin(){

	  $validator = Validator::make(Input::all(),
	    [
	      'email' => 'required',
	      'password' => 'required'
	    ]);

	  if($validator->fails()){
	    return repsonse()->json($validate->errors());
	  }

	  try{
	    if(!$token = auth()->attempt([
	      'email'=> Input::get('email'),
	      'password'=>Input::get('password'),
	    ])
	  ){
	      return response()->json(['error'=>'invalid crediantials']);
	    }
	  } catch(JWTException $e){
	    return response()->json(['error'=>'could not create token']);
	  }
     $user = auth()->user();
    // $user = User::with('roles');
    $role = User::find($user->id)->roles()->orderBy('name')->first();
    if($role->name==='merchant'){
      return redirect('api/merchant/dashboard');
      return response()->json(compact('token'));
    }
    else{
      abort('401','this action is unauthorized');
    }
	}
}
