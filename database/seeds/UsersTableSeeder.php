<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_user = Role::where('name', 'user')->first();
    	$role_admin  = Role::where('name', 'admin')->first();
    	$role_merchant = Role::where('name','merchant')->first();


	    $user = new User();
	    $user->last_name = 'kumar';
	    $user->first_name = 'vishal';
	    $user->mobile_no = '4563217895';
	    $user->email = 'vishal@gmail.com';
	    $user->password = bcrypt('Great9$');
	    $user->status = 1;
	    $user->email_verified_at = 1;
	    $user->save();
	    $user->roles()->attach($role_user);

	    $admin = new User();
	    $admin->last_name = 'bansal';
	    $admin->first_name = 'parteek';
	    $admin->mobile_no = '7009724785';
	    $admin->email = 'parteek@gmail.com';
	    $admin->password = bcrypt('Great9$');
	    $admin->email_verified_at = 1;
	    $admin->save();
	    $admin->roles()->attach($role_admin);

	    $merchant = new User();
	    $merchant->last_name = 'kumar';
	    $merchant->first_name = 'papinder';
	    $merchant->mobile_no = '7009724785';
	    $merchant->email = 'papinder@gmail.com';
	    $merchant->password = bcrypt('Great9$');
	    $merchant->status = 1;
	    $merchant->email_verified_at = 1;
	    $merchant->save();
	    $merchant->roles()->attach($role_merchant);
    }

}
