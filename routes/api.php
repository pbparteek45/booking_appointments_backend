<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/signup','User\EnduserController@signup');

Route::post('signin','User\EnduserController@login');

Route::get('/dashboard','User\EnduserController@dashboard');

Route::get('/verify{token}','User\UserController@VerifyToken');

Route::post('/userstatus/{id}','User\EndUserController@userStatus');



Route::prefix('merchant')->group(function(){

	Route::post('/signup','Merchant\MerchantController@merchantSignup');
	Route::post('/signin','Merchant\MerchantController@merchantLogin');
});

Route::prefix('emp')->group(function(){

	Route::post('/signup','Employee\EmpController@empSignup');
	Route::post('/signin','Employee\EmpController@empLogin');
});

Route::prefix('admin')->group(function(){

	Route::post('/signin','Admin\AdminController@adminLogin');

	Route::get('/userlist','Admin\AdminController@userList');
});